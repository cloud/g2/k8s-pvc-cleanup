# golang

Cleaner for k8s persistent volume chains

In the values.yaml for loki I have this part:

```
loki:
  extraContainers:
  - name: pvcleanup
    image: "[ownRegistry]/pvcleanup:latest"
    env:
      - name: SPACEMONITORING_FOLDER
        value: "/data/loki/chunks"
    volumeMounts:
      - name: storage
        mountPath: "/data"
```
